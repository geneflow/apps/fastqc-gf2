FastQC GeneFlow App
===================

Version: 0.11.9-02

This GeneFlow2 app wraps the FastQC tool.

Inputs
------

1. input: Sequence FASTQ File

Parameters
----------

1. output: Output QC Folder - Output folder that contains QC results
